package com.fixmo.vaadin;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.ArrayUtils;

import com.fixmo.vaadin.widgetset.client.ui.VSimpleTable;
import com.vaadin.data.Container;
import com.vaadin.data.Container.ItemSetChangeEvent;
import com.vaadin.data.Container.PropertySetChangeEvent;
import com.vaadin.data.Container.Sortable;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.data.util.ContainerOrderedWrapper;
import com.vaadin.event.LayoutEvents.LayoutClickEvent;
import com.vaadin.terminal.PaintException;
import com.vaadin.terminal.PaintTarget;
import com.vaadin.terminal.gwt.server.JsonPaintTarget;
import com.vaadin.ui.AbstractComponentContainer;
import com.vaadin.ui.AbstractField;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.ClientWidget;
import com.vaadin.ui.Component;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

@ClientWidget(VSimpleTable.class)
public class SimpleTable extends AbstractComponentContainer implements Container.Viewer, Container.PropertySetChangeListener, 
    Container.ItemSetChangeListener, Field {

    /**
     * Used to create "generated columns"; columns that exist only in the Table,
     * not in the underlying Container. Implement this interface and pass it to
     * Table.addGeneratedColumn along with an id for the column to be generated.
     * 
     */
    public interface ColumnGenerator extends Serializable {

        /**
         * Called by Table when a cell in a generated column needs to be
         * generated.
         * 
         * @param source
         *            the source Table
         * @param itemId
         *            the itemId (aka rowId) for the of the cell to be generated
         * @param columnId
         *            the id for the generated column (as specified in
         *            addGeneratedColumn)
         * @return A {@link Component} that should be rendered in the cell or a {@link String} that should be displayed
         *         in the cell. Other
         *         return values are not supported.
         */
        Component generateCell(final SimpleTable source, final Object itemId, final Object columnId);
    }

    private static final long serialVersionUID = 1L;

    /* Value change events */
    private static final Method VALUE_CHANGE_METHOD;

    static {
        try {
            VALUE_CHANGE_METHOD =
                Property.ValueChangeListener.class.getDeclaredMethod("valueChange",
                    new Class<?>[] { Property.ValueChangeEvent.class });
        }
        catch (final java.lang.NoSuchMethodException e) {
            // This should never happen
            throw new java.lang.RuntimeException("Internal error finding methods in AbstractField");
        }
    }

    private static final int ROW_HEIGHT = 50;

    private static final String BODY_ROW_STYLE_NAME = "table-body-row";

    private static final String HEADER_ROW_STLYE_NAME = "table-header-row";

    private Object sortColumnId;

    private boolean sortAscending = false;

    private Object[] columns = new Object[] {};

    private Map<Object, String> columnHeaders = new HashMap<Object, String>();

    private Container dataSource;

    private boolean selectable = true;

    private HorizontalLayout topPager = new HorizontalLayout();
    
    private final Map<Object, ColumnGenerator> columnGenerators = new HashMap<Object, SimpleTable.ColumnGenerator>();

    private int currentPageFirstItemIndex = 0;

    private final Map<String, Float> expandRatios = new HashMap<String, Float>();

    private final Map<String, Integer> columnWidths = new HashMap<String, Integer>();

    private final Map<String, Object> headerCellContents = new HashMap<String, Object>();

    private final Map<Object, Map<Object, Object>> bodyCellContents = new HashMap<Object, Map<Object, Object>>();

    private int pageLength = 0;

    private Object currentValue;

    private Form editRowForm;

    private Object editableRowItemId;

    private Map<Object, Integer> itemIdToRowIndexMapping = new HashMap<Object, Integer>();

    private HorizontalLayout emptyMessageLayout = new HorizontalLayout();

    private final String defaultEmptyMessage = "There are currently no items in the table.";

    private String emptyMessage;

    private boolean scrolling;

    private final Set<Object> visibleRowIds = new HashSet<Object>();

    private final List<Integer> rowsToRender = new ArrayList<Integer>();

	private final List<Component> components = new ArrayList<Component>();

    public SimpleTable() {
    	//setup the top pager
    	this.topPager.addComponent(new Label("AWERWERWERSSDS"));
    	this.addComponent(this.topPager);
    	this.components.add(this.topPager);
    	
        this.addStyleName("simple-table");
        this.emptyMessageLayout.setMargin(true);
        this.emptyMessageLayout.setVisible(false);
        this.addComponent(this.emptyMessageLayout);
//        this.setComponentAlignment(emptyMessageLayout, Alignment.MIDDLE_CENTER);

        // table needs this listener to update the applicable bulk actions
        this.addListener(new Property.ValueChangeListener() {
            private static final long serialVersionUID = 1L;

            @Override
            public void valueChange(final Property.ValueChangeEvent event) {
//                BetterTable.this.handleTableValueChange();
            }

        });
    }

    /**
     * Formats table cell property values. By default it directly inserts if the property is a component.
     * Otherwise it returns a label with property.toString() or an empty string for null properties.
     * 
     * @param rowId
     *            the Id of the row (same as item Id).
     * @param colId
     *            the Id of the column.
     * @param property
     *            the Property to be formatted.
     * @return the String representation of property and its value.
     * @since 3.1
     */
    // !CHECKSTYLE:DFE:OFF
    protected Object formatPropertyValue(final Object rowId, final Object colId, final Property property) {
        if (property.getValue() == null) {
            return "";
        }
        else if (Component.class.isAssignableFrom(property.getType())) {
            return (Component) property.getValue();
        }
        else {
            return property.getValue().toString();
        }
    }
    // !CHECKSTYLE:DFE:ON

    public final void setVisibleColumns(final String[] visibleColumns) {
        this.setVisibleColumns(Arrays.asList(visibleColumns).toArray());
    }
    
    /**
     * We override this method to ensure that the bulk-action-mode specific
     * column (for the checkboxes) is always present.
     * 
     * @param visibleColumns
     *            the new visible columns
     */
    public final void setVisibleColumns(final Object[] visibleColumns) {

        this.columns = visibleColumns;
        
        // this.recalculateColumnWidths();

        this.refreshBodyCells();
    }

    private Map<Object, Object> getCellContentsForRow(final Object itemId) {
        final Map<Object, Object> cellContents = new HashMap<Object, Object>();
        final Item item = this.dataSource.getItem(itemId);

        if (item == null) {
            throw new 
            RuntimeException("MCGridTable.buildRow(... called for null item.");
        }
        for (Object columnId : this.columns) {
            final Property property = item.getItemProperty(columnId);
            final ColumnGenerator colGenerator = this.getColumnGenerator(columnId);

            // if the current item is in edit mode, see if we can get a field for this property.
            final Object cellContent;
            // resolve the cell contents
            if (colGenerator != null) {
                cellContent = colGenerator.generateCell(this, itemId, columnId);
                if (cellContent == null) {
                    throw new RuntimeException(String.format(
                        "ColumnGenerator for columnId '%s' returned null content.", columnId));
                }
            }
            else if (property != null) {
                cellContent = this.formatPropertyValue(itemId, columnId, property);
            }
            else {
                throw new RuntimeException(String.format("Failure getting cell content for columnId '%s'. All "
                    + "visible columns must be represented by a Container property or a ColumnGenerator", columnId));
            }

            cellContents.put(columnId, cellContent);
        }
        return cellContents;
    }

    public final Object[] getVisibleColumns() {
        return this.columns;
    }

    public final Map<Object, ColumnGenerator> getColumnGenerators() {
        return columnGenerators;
    }

    /**
     * Returns the ColumnGenerator used to generate the given column.
     * 
     * @param columnId
     *            The id of the generated column
     * @return The ColumnGenerator used for the given columnId or null.
     */
    public final ColumnGenerator getColumnGenerator(final Object columnId) {
        return this.columnGenerators.get(columnId);
    }

    public final void addGeneratedColumn(final String columnId, final ColumnGenerator generator) {
        this.getColumnGenerators().put(columnId, generator);
    }

    public final Object createHeaderCellContent(final String columnId) {

        final String headerString = this.getColumnHeader(columnId);

        return (headerString == null) ? columnId : headerString;
    }

    @Override
    public final void containerItemSetChange(final ItemSetChangeEvent event) {
        this.handleDataUpdate();
    }

    private void handleDataUpdate() {
        this.itemIdToRowIndexMapping.clear();
        // setCurrentPageFirstItemIndex ensures that we aren't serving an inexistent page (i.e. after an item removal)
        setCurrentPageFirstItemIndex(this.getCurrentPageFirstItemIndex());
        this.refreshBodyCells();
    }

    @Override
    public final void containerPropertySetChange(final PropertySetChangeEvent event) {
        this.handleDataUpdate();
    }

    // !CHECKSTYLE:DFE:OFF
    @Override
    public void setContainerDataSource(final Container newDataSource) {

        if (this.dataSource == null || !this.dataSource.equals(newDataSource)) {

            // remove the old listeners
            if (this.dataSource != null) {
                if (this.dataSource instanceof Container.ItemSetChangeNotifier) {
                    ((Container.ItemSetChangeNotifier) this.dataSource).removeListener(this);
                }
                if (this.dataSource instanceof Container.PropertySetChangeNotifier) {
                    ((Container.PropertySetChangeNotifier) this.dataSource).removeListener(this);
                }
                this.dataSource = null;
            }

            if (newDataSource != null) {

                // set the new Container..
                this.dataSource =
                    (Container.Ordered.class.isAssignableFrom(newDataSource.getClass()))
                        ? newDataSource
                        : new ContainerOrderedWrapper(newDataSource);

                // add the new listeners
                if (this.dataSource instanceof Container.ItemSetChangeNotifier) {
                    ((Container.ItemSetChangeNotifier) this.dataSource).addListener(this);
                }
                if (this.dataSource instanceof Container.PropertySetChangeNotifier) {
                    ((Container.PropertySetChangeNotifier) this.dataSource).addListener(this);
                }
            }

            // Reset page position
            this.setCurrentPageFirstItemIndex(0);
            this.refreshBodyCells();
        }
    }
    // !CHECKSTYLE:DFE:ON

    /**
     * Sorts the table.
     * 
     * @throws UnsupportedOperationException
     *             if the container data source does not implement
     *             Container.Sortable
     * @see com.vaadin.data.Container.Sortable#sort(java.lang.Object[], boolean[])
     * 
     */
    public final void sort(final Object[] propertyId, final boolean[] ascending) {
        final Container c = this.getContainerDataSource();
        if (c instanceof Container.Sortable) {
            final int pageIndex = this.getCurrentPageFirstItemIndex();
            ((Container.Sortable) c).sort(propertyId, ascending);
            this.setCurrentPageFirstItemIndex(pageIndex);
        }
        else if (c != null) {
            throw new UnsupportedOperationException("Underlying Container does not allow sorting");
        }
        this.updateSortIndicator();
    }

    private boolean isRowSelected(final Object itemId) {
        if (this.getValue() != null) {
            if (this.getValue().equals(itemId) || false // this.isBatchMode()
                && ((Collection<?>) this.getValue()).contains(itemId)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public final void paintContent(final PaintTarget target) throws PaintException {
        
        target.addAttribute(VSimpleTable.ATTRIBUTE_ROW_COUNT, this.visibleRowIds.size());
        target.addAttribute(VSimpleTable.ATTRIBUTE_VISIBLE_COLUMNS, this.columns);
        
        this.paintChildren(target);
        
        this.paintRows(target, this.getVisibleRows());

    }

    private void paintChildren(final PaintTarget target) throws PaintException {
        // Adds all items in all the locations
        for (Component c : components) {
            // Paint child component UIDL
            c.paint(target);
        }
		
	}

	private void paintRows(final PaintTarget target, final Object[][] cells) throws PaintException {

        target.startTag("rows");

        for (int rowIndex = 0; rowIndex < cells.length; rowIndex++) {

            target.startTag("tr");

            // cells
            for (int currentColumn = 0; currentColumn < this.columns.length; currentColumn++) {

                target.startTag("td");
                if (Component.class.isAssignableFrom(cells[rowIndex][currentColumn].getClass())) {
                    final Component c = (Component) cells[rowIndex][currentColumn];
                    target.addAttribute("iscomponent", true);
                    c.setParent(null);
                    c.paint(target);
                }
                else {
                    target.addText((String) cells[rowIndex][currentColumn]);
                }
                target.endTag("td");
            }

            target.endTag("tr");
        }
        target.endTag("rows");
    }

    private Object[][] getVisibleRows() {
        final Object[][] rows = new Object[this.visibleRowIds.size()][this.columns.length];
        for (final Object itemId : this.visibleRowIds) {
            final Map<Object, Object> rowContents = this.bodyCellContents.get(itemId);
            for (int columnIndex = 0; columnIndex < this.columns.length; columnIndex++) {
                rows[this.itemIdToRowIndexMapping.get(itemId)][columnIndex] 
                    = rowContents.get(this.columns[columnIndex]);
            }
        }
        return rows;
    }

    private void refreshRowCells(final Object itemId) {
        this.refreshBodyCells(); // TODO: improve!
        // final Map<String, Component> contents = this.buildRow(itemId);
        // final Iterator<Component> iter = this.rowContainer.getComponentIterator();
        // while (iter.hasNext()) {
        // final Component c = iter.next();
        // if (((AbstractComponent) c).getData().equals(itemId)) {
        // this.rowContainer.replaceComponent(c, this.buildRow(itemId));
        // break;
        // }
        // }
    }

    private void refreshBodyCells() {
        final int dataSetSize = (this.dataSource == null) ? 0 : this.dataSource.size();
        final int visibleRowsOnPage =
            (this.getPageLength() == 0) ? dataSetSize : Math.min(this.getPageLength(),
                (dataSetSize - this.currentPageFirstItemIndex));
        final int lastRowIndex = (visibleRowsOnPage + this.currentPageFirstItemIndex);
        this.visibleRowIds.clear();
        this.rowsToRender.clear();
        for (int rowIndex = this.currentPageFirstItemIndex; rowIndex < lastRowIndex; rowIndex++) {
            this.rowsToRender.add(rowIndex);
            final Object itemId = ((Container.Indexed) this.dataSource).getIdByIndex(rowIndex);
            this.visibleRowIds.add(itemId);
            this.itemIdToRowIndexMapping.put(itemId, rowIndex);
            
//            final int visiblePosition = (this.itemIdToRowIndexMapping.get(itemId) - this.currentPageFirstItemIndex);

            // recomputes the cell contents, which for generated columns means the content generation is run,
            // and for regular columns (those that pertain to "properties" represented by a container) the
            // getValue() method of the property is called again.
            this.bodyCellContents.put(itemId, this.buildRow(itemId));

        }

        this.emptyMessageLayout.removeAllComponents();
        this.emptyMessageLayout.setVisible(false);
        if (dataSetSize == 0) {
            this.emptyMessageLayout.setVisible(true);
            final String msg =
                (this.getEmptyMessage() == null) ? this.getDefaultEmptyMessage() : this.getEmptyMessage();
//            this.emptyMessageLayout.addComponent(new Label(TM.get(msg)));
            // Don't allow table to scroll if there is no data, otherwise it will not take up the full space.
            // This depends on the table setting its expand ratios properly (which have no effect with scrolling on).
//            this.setScrollingOnTable(false);
        }
    }

    protected final void handleHeaderLayoutClick(final LayoutClickEvent event) {
        final Component childComponent = event.getChildComponent();
        if (childComponent != null) {
            final Object columnId = ((VerticalLayout) childComponent).getData();
            if (Sortable.class.isAssignableFrom(this.getContainerDataSource().getClass())
                && ((Sortable) this.getContainerDataSource()).getSortableContainerPropertyIds().contains(columnId)) {
                if (columnId.equals(this.sortColumnId)) {
                    // flip the sort (ASC -> DESC)
                    this.sortAscending = !this.sortAscending;
                }
                else {
                    this.sortAscending = true;
                }
                this.sortColumnId = columnId;
                this.sort(new Object[] { this.sortColumnId }, new boolean[] { this.sortAscending });
            }
        }
    }

    public final void updateSortIndicator() {
        // for (String colId : this.columns) {
        // final Component c = this.headerCellContainers.get(colId);
        // c.removeStyleName(ASCENDING_SORT_INDICATOR);
        // c.removeStyleName(DESCENDING_SORT_INDICATOR);
        // if (colId.equals(this.sortColumnId)) {
        // c.addStyleName(this.sortAscending ? ASCENDING_SORT_INDICATOR : DESCENDING_SORT_INDICATOR);
        // }
        // }
    }

    private Map<Object, Field> buildFormForRow(final Object itemId) {
        final Map<Object, Field> formFields = new HashMap<Object, Field>();
//        if (itemId.equals(this.getEditableRowItemId()) && this.getRowEditorDelegate() != null) {
//            final Item item = this.dataSource.getItem(itemId);
//            this.editRowForm = new Form();
//            this.editRowForm.setFormFieldFactory(new FormFieldFactory() {
//                private static final long serialVersionUID = 1L;
//
//                @Override
//                public Field createField(final Item item, final Object propertyId, final Component uiContext) {
//                    return formFields.get(propertyId);
//                }
//            });
//
//            for (Object columnId : item.getItemPropertyIds()) {
//                // if the current item is in edit mode, see if we can get a field for this property.
////                final Field field = this.getRowEditorDelegate().createField(item, columnId, this);
////                if (field != null) {
////                    formFields.put(columnId, field);
////                }
//            }
//            this.editRowForm.setItemDataSource(item);
//        }
        return formFields;
    }

    private Map<Object, Object> buildRow(final Object itemId) {
        final Map<Object, Object> cells = new HashMap<Object, Object>();
        final Map<Object, Field> formFields = this.buildFormForRow(itemId);
        final Map<Object, Object> cellContents = this.getCellContentsForRow(itemId);

        final List<Object> cols = Arrays.asList(this.columns);
        for (Object columnId : cols.toArray(new String[] {})) {
            final Object cellContent;
            if (formFields.containsKey(columnId)) {
                cellContent = formFields.get(columnId);
            }
            else {
                cellContent = cellContents.get(columnId);
            }

            cells.put(columnId, cellContent);

            // wrappedCell.setDebugId("row-" + (visiblePosition + 1) + ".column-" + columnId);
            // wrappedCell.addListener(this.rowClickListener);

        }
        return cells;
    }

    @Override
    public final Container getContainerDataSource() {
        return this.dataSource;
    }

    @Override
    public final boolean isInvalidCommitted() {
        return false;
    }

    @Override
    public final void setInvalidCommitted(final boolean isCommitted) {
        throw new UnsupportedOperationException("Tables are dumb fields!");
    }

    @Override
    public final void commit() {
        throw new UnsupportedOperationException("Tables are dumb fields!");
    }

    @Override
    public final void discard() {
        throw new UnsupportedOperationException("Tables are dumb fields!");
    }

    @Override
    public final boolean isWriteThrough() {
        throw new UnsupportedOperationException("Tables are dumb fields!");
    }

    @Override
    public final void setWriteThrough(final boolean writeThrough) {
        throw new UnsupportedOperationException("Tables are dumb fields!");
    }

    @Override
    public final boolean isReadThrough() {
        throw new UnsupportedOperationException("Tables are dumb fields!");
    }

    @Override
    public final void setReadThrough(final boolean readThrough) {
        throw new UnsupportedOperationException("Tables are dumb fields!");
    }

    @Override
    public final boolean isModified() {
        throw new UnsupportedOperationException("Tables are dumb fields!");
    }

    @Override
    public final void addValidator(final Validator validator) {
        throw new UnsupportedOperationException("Tables are dumb fields!");
    }

    @Override
    public final void removeValidator(final Validator validator) {
        throw new UnsupportedOperationException("Tables are dumb fields!");
    }

    @Override
    public final Collection<Validator> getValidators() {
        return null;
    }

    @Override
    public final boolean isValid() {
        return true;
    }

    @Override
    public final void validate() {
        throw new UnsupportedOperationException("Tables are dumb fields!");
    }

    @Override
    public final boolean isInvalidAllowed() {
        return true;
    }

    @Override
    public final void setInvalidAllowed(final boolean invalidValueAllowed) {
        throw new UnsupportedOperationException("Tables are dumb fields!");
    }

    @Override
    public final Object getValue() {
        final Object retValue = this.currentValue;

//        if (this.isBatchMode()) {
        if (false) {

            // If the return value is not a set
            if (retValue == null) {
                return new HashSet<Object>();
            }
            if (retValue instanceof Set) {
                @SuppressWarnings("unchecked")
                final Set<Object> visibleAndSelected = new HashSet<Object>((Set<Object>) retValue);
                visibleAndSelected.retainAll(this.visibleRowIds);
                return Collections.unmodifiableSet(visibleAndSelected);
            }
            else if (retValue instanceof Collection) {
                return new HashSet<Object>((Collection<?>) retValue);
            }
            else {
                final Set<Object> s = new HashSet<Object>();
                if (this.dataSource.containsId(retValue)) {
                    s.add(retValue);
                }
                return s;
            }

        }
        else {
            return retValue;
        }
    }

    @Override
    public final void setValue(final Object newValue) {
        if (false) { //this.isBatchMode()) {
            if (newValue == null) {
                this.currentValue = new LinkedHashSet<Object>();
            }
            else if (Collection.class.isAssignableFrom(newValue.getClass())) {
                this.currentValue = new LinkedHashSet<Object>((Collection<?>) newValue);
            }
        }
        else if (newValue == null || this.dataSource.containsId(newValue)) {
            this.currentValue = newValue;
        }

        this.fireValueChange();
    }

    /**
     * Emits the value change event. The value contained in the field is
     * validated before the event is created.
     */
    protected final void fireValueChange() {
        fireEvent(new AbstractField.ValueChangeEvent(this));
    }

    @Override
    public final Class<?> getType() {
        throw new UnsupportedOperationException("Tables are dumb fields!");
    }

    /*
     * Adds a value change listener for the field. Don't add a JavaDoc comment
     * here, we use the default documentation from the implemented interface.
     */
    public final void addListener(final Property.ValueChangeListener listener) {
        addListener(AbstractField.ValueChangeEvent.class, listener, VALUE_CHANGE_METHOD);
    }

    /*
     * Removes a value change listener from the field. Don't add a JavaDoc
     * comment here, we use the default documentation from the implemented
     * interface.
     */
    public final void removeListener(final Property.ValueChangeListener listener) {
        removeListener(AbstractField.ValueChangeEvent.class, listener, VALUE_CHANGE_METHOD);
    }

    /**
     * Removes a PropertySetChangeListener from the underlying container.
     * 
     */
    public final void removeListener(final Container.PropertySetChangeListener listener) {
        if (this.dataSource != null
            && Container.PropertySetChangeNotifier.class.isAssignableFrom(this.dataSource.getClass())) {
            ((Container.PropertySetChangeNotifier) this.dataSource).removeListener(listener);
        }
    }

    /**
     * Adds a PropertySetChangeListener to the underlying container.
     * 
     */
    public final void addListener(final Container.PropertySetChangeListener listener) {
        if (this.dataSource != null
            && Container.PropertySetChangeNotifier.class.isAssignableFrom(this.dataSource.getClass())) {
            ((Container.PropertySetChangeNotifier) this.dataSource).addListener(listener);
        }
    }

    /**
     * Removes an ItemSetChangeListener from the underlying container.
     * 
     */
    public final void removeListener(final Container.ItemSetChangeListener listener) {
        if (this.dataSource != null
            && Container.ItemSetChangeNotifier.class.isAssignableFrom(this.dataSource.getClass())) {
            ((Container.ItemSetChangeNotifier) this.dataSource).removeListener(listener);
        }
    }

    /**
     * Adds an ItemSetChangeListener to the underlying container.
     * 
     */
    public final void addListener(final Container.ItemSetChangeListener listener) {
        if (this.dataSource != null
            && Container.ItemSetChangeNotifier.class.isAssignableFrom(this.dataSource.getClass())) {
            ((Container.ItemSetChangeNotifier) this.dataSource).addListener(listener);
        }
    }

    @Override
    public final void valueChange(final Property.ValueChangeEvent event) {
        throw new UnsupportedOperationException("Tables are dumb fields!");
    }

    @Override
    public final void setPropertyDataSource(final Property newDataSource) {
        throw new UnsupportedOperationException("Tables are dumb fields!");
    }

    @Override
    public final Property getPropertyDataSource() {
        throw new UnsupportedOperationException("Tables are dumb fields!");
    }

    @Override
    public final boolean isRequired() {
        return false;
    }

    @Override
    public final void setRequired(final boolean required) {
        throw new UnsupportedOperationException("Tables are dumb fields!");
    }

    @Override
    public final void setRequiredError(final String requiredMessage) {
        throw new UnsupportedOperationException("Tables are dumb fields!");
    }

    @Override
    public final String getRequiredError() {
        throw new UnsupportedOperationException("Tables are dumb fields!");
    }

    public final Container getDataSource() {
        return dataSource;
    }

    public final int getCurrentPageFirstItemIndex() {
        return currentPageFirstItemIndex;
    }

    public final void setCurrentPageFirstItemIndex(final int currentPageFirstItemIndex) {
        final int previousPageFirstItemIndex = this.currentPageFirstItemIndex;
        this.currentPageFirstItemIndex = currentPageFirstItemIndex;
        final int pageCount = calculatePageCount();

        /**
         * This appears to account for the possibility that the container has shrunk in size.
         * In that scenario we should just should the last page of results.
         * 
         */
        if (calculateCurrentPage() > pageCount) {
            this.currentPageFirstItemIndex = (pageCount - 1) * this.getPageLength();
        }

        /**
         * We only trigger the recalculation of cell contents if the page actually changed.
         */
        if (previousPageFirstItemIndex != this.currentPageFirstItemIndex) {
            this.refreshBodyCells();
            // Apply the page number to both pagination components. The calculation is the opposite to that from
            // handlePage in PagingControlBar
            final int currentPage = this.currentPageFirstItemIndex / this.getPageLength() + 1;
//            bottomPagingComponent.changePageNumber(currentPage);
//            topPagingComponent.changePageNumber(currentPage);
        }
    }

    public final int getPageLength() {
        return pageLength;
    }

    public final void setPageLength(final int pageLength) {
        this.pageLength = pageLength;
    }

    public final Map<String, Float> getExpandRatios() {
        return expandRatios;
    }

    public final boolean isSelectable() {
        return selectable;
    }

    public final void setSelectable(final boolean selectable) {
        this.selectable = selectable;
    }

    public final Item getItem(final Object itemId) {
        if (this.getContainerDataSource() != null) {
            return this.getContainerDataSource().getItem(itemId);
        }
        return null;
    }

    public final void removeItem(final Object itemId) {
        if (this.getContainerDataSource() != null) {
            this.getContainerDataSource().removeItem(itemId);
        }
    }

    public final void removeAllItems() {
        if (this.getContainerDataSource() != null) {
            this.getContainerDataSource().removeAllItems();
        }
    }

    public final Object getContainerPropertyIds() {
        if (this.getContainerDataSource() != null) {
            return this.getContainerDataSource().getContainerPropertyIds();
        }
        return new Object[] {};
    }

    public final Map<Object, String> getColumnHeaders() {
        return columnHeaders;
    }

    public final String getColumnHeader(final String columnId) {
        return this.columnHeaders.get(columnId);
    }

    public final void setColumnHeaders(final String[] colHeaders) {
        this.columnHeaders.clear();

        for (int i = 0; i < colHeaders.length; i++) {
            this.columnHeaders.put(this.columns[i], colHeaders[i]);
        }

//        this.regenerateHeaderCells(this.columns);
    }

    public final void setColumnExpandRatio(final String columnId, final float ratio) {
        final int index = ArrayUtils.indexOf(this.columns, columnId);
        if (index < 0) {
            throw new RuntimeException(String.format("Table does not contain column represented by '%s'", columnId));
        }
        this.getExpandRatios().put(columnId, ratio);
        // this.recalculateColumnWidths();
    }

    public final void setColumnWidth(final String columnId, final int pixels) {
        final int index = ArrayUtils.indexOf(this.columns, columnId);
        if (index < 0) {
            throw new RuntimeException(String.format("Table does not contain column represented by '%s'", columnId));
        }
        this.getColumnWidths().put(columnId, pixels);
        // this.recalculateColumnWidths();
    }

    public final Object getEditableRowItemId() {
        return editableRowItemId;
    }

    public final void setEditableRowItemId(final Object editableRowItemId) {
        this.editableRowItemId = editableRowItemId;
        this.refreshRowCells(editableRowItemId);
    }

    public final Map<String, Integer> getColumnWidths() {
        return columnWidths;
    }

    public final void setEmptyMessage(final String emptyMessage) {
        this.emptyMessage = emptyMessage;
        if (this.dataSource != null && this.dataSource.size() <= 0) {
            this.refreshBodyCells();
        }
    }

    public final String getEmptyMessage() {
        return this.emptyMessage;
    }

    public final String getDefaultEmptyMessage() {
        return defaultEmptyMessage;
    }

    /**
     * Sets whether the table should expand (scrolling off) or scroll (does not expand)
     * 
     * @param scrolling
     *            whether the table should scroll or not
     */
    public final void setScrolling(final boolean scrolling) {
        this.scrolling = scrolling;
    }

    public final boolean getScrolling() {
        return scrolling;
    }

    /**
     * Calculate page using this.currentPageFirstItemIndex.
     * 
     * @return the int
     */
    public final int calculateCurrentPage() {
        if (this.getPageLength() > 0 && this.getTableSize() > 0) {
            return (int) Math.floor(this.currentPageFirstItemIndex / this.getPageLength()) + 1;
        }
        return 1;
    }

    public final int getTableSize() {
        if (this.getContainerDataSource() != null) {
            return this.getContainerDataSource().size();
        }
        return 0;
    }

    /**
     * Calculate total number of pages.
     * 
     * @return the int PageCount
     */
    public final int calculatePageCount() {
        if (this.getPageLength() > 0 && this.getTableSize() > 0) {

            if (this.getTableSize() % this.getPageLength() == 0) {
                return this.getTableSize() / this.getPageLength();
            }
            else {
                return this.getTableSize() / this.getPageLength() + 1;
            }

        }
        return 1;
    }

    public final Map<Object, Map<Object, Object>> getBodyCellContents() {
        return bodyCellContents;
    }

    public final Map<String, Object> getHeaderCellContents() {
        return headerCellContents;
    }

    @Override
    public final int getTabIndex() {
        return 0;
    }

    @Override
    public void setTabIndex(final int tabIndex) {
    }

    @Override
    public final void focus() {
        super.focus();
    }

	@Override
	public void replaceComponent(Component oldComponent, Component newComponent) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Iterator<Component> getComponentIterator() {
		return this.components.iterator();
	}
}
