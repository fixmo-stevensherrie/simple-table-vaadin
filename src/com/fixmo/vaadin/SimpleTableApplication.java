package com.fixmo.vaadin;

import java.math.BigInteger;
import java.security.SecureRandom;

import com.vaadin.Application;
import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.ui.*;

/**
 * Main application class.
 */
public class SimpleTableApplication extends Application {
	private static final long serialVersionUID = 1L;

	public final static String GENERATED_COL_ID = "generated_column";
	public final static String[] CONTAINER_PROPERTIES = new String[] { "Property 1", "Property 2", "Property 3", "Property 4", "Property 5", GENERATED_COL_ID};
	private final static SecureRandom random = new SecureRandom();

	@Override
	public void init() {
		Window mainWindow = new Window("Bettervaadintable Application");
		mainWindow.addComponent(new Label("Table test"));
		mainWindow.addComponent(this.buildBetterTableDemo());
		setMainWindow(mainWindow);
	}

	public SimpleTable buildBetterTableDemo() {
		final SimpleTable table = new SimpleTable();
		table.addGeneratedColumn(GENERATED_COL_ID, new SimpleTable.ColumnGenerator() {
			
			private static final long serialVersionUID = 1L;

			@Override
			public Component generateCell(SimpleTable source, Object itemId, Object columnId) {
				final HorizontalLayout hl = new HorizontalLayout();
				hl.addComponent(new Button("POOP"));
				return hl;
			}
		});
		this.populateBetterTable(table);
		table.addComponent(new Label("WOOOOOOOOP!"));
		return table;
	}

	private void populateBetterTable(SimpleTable table) {
		final IndexedContainer ic = new IndexedContainer();
		for (Object propertyId : CONTAINER_PROPERTIES) {
			ic.addContainerProperty(propertyId, String.class, null);
		}
		int i = 0;
		while (i < 10) {
			i++;
			final Object itemId = ic.addItem();
			final Item item = ic.getItem(itemId);
			for (Object propertyId : CONTAINER_PROPERTIES) {
				item.getItemProperty(propertyId).setValue(this.getRandomValue());
			}
		}
		table.setContainerDataSource(ic);
		table.setVisibleColumns(CONTAINER_PROPERTIES);
		table.setColumnHeaders(CONTAINER_PROPERTIES);
	}

	private Object getRandomValue() {
		return new BigInteger(130, random).toString(32);
	}
}

