package com.fixmo.vaadin.widgetset.client.ui;

import java.util.Iterator;
import java.util.Set;

import com.google.gwt.user.client.ui.Widget;
import com.vaadin.terminal.gwt.client.ApplicationConnection;
import com.vaadin.terminal.gwt.client.Paintable;
import com.vaadin.terminal.gwt.client.RenderSpace;
import com.vaadin.terminal.gwt.client.UIDL;
import com.vaadin.terminal.gwt.client.ui.layout.CellBasedLayout;
import com.vaadin.terminal.gwt.client.ui.layout.ChildComponentContainer;

public class VSimpleTable extends CellBasedLayout implements Paintable {

    public static final String ATTRIBUTE_ROW_COUNT = "rowCount";
    public static final String ATTRIBUTE_VISIBLE_COLUMNS = "visibleColumns";
    public static final String ATTRIBUTE_ROW_CONTAINER = "rowContainer";

    private final VSimpleTableBody tableBody = new VSimpleTableBody();
    private ApplicationConnection client;
    private final ChildComponentContainer bodyContainer = new ChildComponentContainer(this.tableBody, 0);
    private ChildComponentContainer topPagerContainer;
    private String topPagePID; 

    public VSimpleTable() {
        this.addOrMoveChild(this.bodyContainer, 0);
    }

    @Override
    public final void updateFromUIDL(final UIDL uidl, final ApplicationConnection client) {
        this.client = client;
        /*
         * This call should be made first. Ensure correct implementation, handle
         * size etc.
         */
        if (client.updateComponent(this, uidl, true)) {
            return;
        }

        this.tableBody.removeAllRows();
        this.topPagePID = uidl.getStringAttribute("topPager");

        final UIDL rowUIDL = uidl.getChildByTagName("rows");
//        final UIDL headerUIDL = uidl.getChildByTagName("headerCells");
//        final UIDL bottomPagerUIDL = uidl.getChildByTagName("bottomPager");
//        final UIDL batchActionBarUIDL = uidl.getChildByTagName("batchActionBar");
//        if (uidl.hasAttribute("topPager")) {
//        final UIDL topPagerUIDL = uidl.getChildByTagName("topPager");

        for (int i = 0; i < uidl.getChildCount(); i++) {
        	final UIDL childUIDL = uidl.getChildUIDL(i);
        	final String pid = childUIDL.getId();
        	if (this.topPagePID != null && this.topPagePID.equals(pid)) {
        		Paintable p = client.getPaintable(childUIDL);
        		Widget w = (Widget) p;
        		if (this.topPagerContainer == null) {
        			this.topPagerContainer = new ChildComponentContainer(w, 0);
        		}
        		else {
        			this.topPagerContainer.setWidget(w);
        		}
        		this.topPagerContainer.renderChild(childUIDL, client, -1);
        		this.topPagerContainer.updateWidgetSize();
        		this.addOrMoveChild(this.topPagerContainer, 0);
        	}
        }
        
//    	final Paintable topPagerPaintable = client.getPaintable(pid);
        // updates the rows of the table
        this.renderRows(rowUIDL);

        // updates the header cells
//        this.renderHeader(headerUIDL);
        
        this.bodyContainer.updateWidgetSize();
        this.calculateContainerSize();
    }
    
    private void renderHeader(UIDL headerUIDL) {
		// TODO Auto-generated method stub
		
	}

	protected void renderRows(UIDL rowUIDL) {
    	this.bodyContainer.renderChild(rowUIDL, client, -1);
    }
    
    private void calculateContainerSize() {

        /*
         * Container size here means the size the container gets from the
         * component. The expansion size is not include in this but taken
         * separately into account.
         */
        int height = 0;
        int width = 0;
        final Iterator<Widget> widgetIterator = iterator();
        width = this.tableBody.getOffsetWidth();
        while (widgetIterator.hasNext()) {
            final ChildComponentContainer childComponentContainer = (ChildComponentContainer) widgetIterator.next();

            if (!childComponentContainer.isComponentRelativeSized(0)) {
                /*
                 * Only components with non-relative size in the main
                 * direction has a container size
                 */
                height = childComponentContainer.getWidgetSize().getHeight()
                    + childComponentContainer.getCaptionHeightAboveComponent();
            }
            else {
                height = 0;
            }

            childComponentContainer.setContainerSize(width, height);
        }

    }

    @Override
    public void updateCaption(Paintable component, UIDL uidl) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public boolean requestLayout(Set<Paintable> children) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public RenderSpace getAllocatedSpace(Widget child) {
        // TODO Auto-generated method stub
        return null;
    }
    
}
