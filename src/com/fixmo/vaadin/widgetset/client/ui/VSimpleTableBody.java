package com.fixmo.vaadin.widgetset.client.ui;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Widget;
import com.vaadin.terminal.gwt.client.ApplicationConnection;
import com.vaadin.terminal.gwt.client.Container;
import com.vaadin.terminal.gwt.client.Paintable;
import com.vaadin.terminal.gwt.client.RenderSpace;
import com.vaadin.terminal.gwt.client.UIDL;
import com.vaadin.terminal.gwt.client.VConsole;
import com.vaadin.terminal.gwt.client.ui.VLabel;

public class VSimpleTableBody extends FlexTable implements Container {

    public ApplicationConnection client;

    private List<UIDL> pendingComponentPaints;
    protected Map<Widget, Integer[]> widgetToPosition = new HashMap<Widget, Integer[]>();

    public VSimpleTableBody() {
    }

    @Override
    public void updateFromUIDL(UIDL uidl, ApplicationConnection client) {
        this.client = client;
        for (int rowIndex = 0; rowIndex < uidl.getChildCount(); rowIndex++) {
            final UIDL rowUIDL = uidl.getChildUIDL(rowIndex);
            for (int columnIndex = 0; columnIndex < rowUIDL.getChildCount(); columnIndex++) {
                final UIDL cellUIDL = rowUIDL.getChildUIDL(columnIndex);
                final boolean isComponent = cellUIDL.hasAttribute("iscomponent");
                Widget cellContent = (isComponent) ? (Widget) client.getPaintable(cellUIDL.getChildUIDL(0)) : 
                    new VLabel(cellUIDL.getChildString(0));
                this.setWidget(rowIndex, columnIndex, cellContent);
                this.widgetToPosition.put(cellContent, new Integer[] { rowIndex, columnIndex });

                if (isComponent) {
                    paintComponent((Paintable) cellContent, cellUIDL.getChildUIDL(0));
                }
            }
        }
    }

    protected final void paintComponent(final Paintable p, final UIDL uidl) {
        if (isAttached()) {
            p.updateFromUIDL(uidl, client);
//            client.updateComponent((Widget) p, uidl, false);
        }
        else {
            if (pendingComponentPaints == null) {
                pendingComponentPaints = new LinkedList<UIDL>();
            }
            pendingComponentPaints.add(uidl);
        }
    }
    
    @Override
    public void replaceChildComponent(Widget oldComponent, Widget newComponent) {
        final Integer[] position = this.widgetToPosition.get(oldComponent);
        if (position != null) {
            final Integer rowIndex = position[0]; 
            final Integer columnIndex = position[1];
            this.widgetToPosition.remove(oldComponent);
            this.setWidget(rowIndex, columnIndex, newComponent);
            this.widgetToPosition.put(newComponent, position);
        }
    }

    @Override
    public boolean hasChildComponent(Widget component) {
        return this.widgetToPosition.containsKey(component);
    }

    @Override
    public void updateCaption(Paintable component, UIDL uidl) {
        // noop
    }

    @Override
    public boolean requestLayout(Set<Paintable> children) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public RenderSpace getAllocatedSpace(Widget child) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected final void onAttach() {
        super.onAttach();
        if (pendingComponentPaints != null) {
            VConsole.log("Executing deferred component painting.....");
            for (UIDL uidl : pendingComponentPaints) {
                final Paintable paintable = client.getPaintable(uidl);
                paintable.updateFromUIDL(uidl, client);
            }
            pendingComponentPaints.clear();
        }
    }

}
